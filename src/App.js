import logo from './logo.svg';
import './App.css';
import { Button } from 'react-bootstrap';
import Movies from './components/movies';

function App() {
  return (
    <main className="container">
        <Movies />
    </main>
  );
}

export default App;
