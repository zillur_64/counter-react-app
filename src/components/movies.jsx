import React, { Component } from 'react';
import {getMovies} from '../services/fakeMovieService';
import Pagination from './common/pagination';
import {paginate} from '../utils/paginate';
import ListGroup from './common/listGroup';
import {getGenres} from '../services/fakeGenreService';
import MoviesTable from './moviesTable';
import _ from 'lodash';

class Movies extends Component {
    state = {
        movies: [],
        genres: [],
        currentPage: 1,
        pageSize: 7,
        sortColumn: {path: 'title', order: 'asc'}
    };
    componentDidMount(){
        const genres = [{_id: "", name: 'All Genre'}, ...getGenres()]
        this.setState({movies: getMovies(), genres: genres});
    }
    handleDelete = (movie) =>{
        const movies = this.state.movies.filter(m =>m._id !== movie._id);
        this.setState({movies: movies});
    }
    handleLike = (movie) =>{
        const movies = [...this.state.movies];
        const index = movies.indexOf(movie);
        movies[index] = {...movies[index]};
        movies[index].liked =! movies[index].liked;
        this.setState({movies});
    };
    handlePageChange = page =>{
        this.setState({currentPage: page})
    }
    handleGenreSelect = genre =>{
        this.setState({selectedGenre : genre, currentPage:1})
    }
    handleSort = sortColumn =>{
        this.setState({sortColumn});
    }
    render() { 
        const {length: count} = this.state.movies;
        const {pageSize, currentPage, movies : allMovies, selectedGenre, sortColumn} = this.state;
        const filtered = selectedGenre && selectedGenre._id ? allMovies.filter(m => m.genre._id === selectedGenre._id) : allMovies;
        const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);
        const movies = paginate(sorted, currentPage, pageSize);
        if(count === 0) 
        return <p>There are no movies in database</p>;
        
        return (
        <div className="row">
            <div className="col-2">
                <ListGroup 
                    items={this.state.genres}
                    onItemSelect={this.handleGenreSelect}
                    selectedItem={this.state.selectedGenre}
                />
            </div>
            <div className="col">
                <p>Showing {filtered.length} movies in database</p>
                <MoviesTable 
                    movies={movies} 
                    sortColumn={sortColumn}
                    onLike={this.handleLike} 
                    onDelete={this.handleDelete}
                    onSort={this.handleSort}
                />
                <Pagination 
                    itemsCount={filtered.length} 
                    pageSize={pageSize}
                    currentPage={currentPage} 
                    onPageChange={this.handlePageChange}
                />
            </div>
        </div>
        );
    }
}
 
export default Movies;
