import React, { Component } from 'react';
import Like from './common/like'; 
import TableHeader from './common/tableHeader';
import TableBody from './common/tableBody';
import _ from 'lodash';

class MoviesTable extends Component {
    columns = [
        { path: 'title', label: 'Title'},
        { path: 'genre.name', label: 'Label'},
        { path: 'numberInStock', label: 'Stock'},
        { path: 'dailyRentalRate', label: 'Rate'},
        { 
            key: 'Like',
            content: movie => (<Like liked={movie.liked } onClick ={() => this.props.onLike(movie)}/>)
        },
        { 
            key: 'Delete',
            content:  movie => (<button onClick = {() => this.props.onDelete(movie)} className="btn btn-danger btn-sm">Delete</button>)
        }
    ];
    render() { 
        const {movies, onLike, onDelete, onSort, sortColumn} = this.props;
        return ( 
        <table className="table">
            <TableHeader 
                columns={this.columns}
                sortColumn={sortColumn}
                onSort={onSort}
            />
            <TableBody data={movies} columns= {this.columns}/>
        </table> );
    }
}

 
export default MoviesTable;