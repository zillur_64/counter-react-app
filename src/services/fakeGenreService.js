export const genres = [
  { _id: "5b21ca3eeb7f6fbccd471818", name: "Action" },
  { _id: "5b21ca3eeb7f6fbccd471814", name: "Comedy" },
  { _id: "5b21ca3eeb7f6fbccd471820", name: "Thriller" },
  { _id: "5b21ca3eeb7f6fbccd471821", name: "Romantic" },
  { _id: "5b21ca3eeb7f6fbccd471822", name: "Adventure" },
  { _id: "5b21ca3eeb7f6fbccd471825", name: "Love" },
  { _id: "5b21ca3eeb7f6fbccd471829", name: "Other" }
];

export function getGenres() {
  return genres.filter(g => g);
}
